package cn.itcast.common.fdfs;

import java.io.IOException;

import org.apache.commons.io.FilenameUtils;
import org.csource.common.NameValuePair;
import org.csource.fastdfs.ClientGlobal;
import org.csource.fastdfs.StorageClient1;
import org.csource.fastdfs.TrackerClient;
import org.csource.fastdfs.TrackerServer;
import org.springframework.core.io.ClassPathResource;

public class FdfsUtils {

	public static String uploadPic(byte[] pic,String filename,Long size) throws Exception{
		//获得tracker的ip
		ClassPathResource resource = new ClassPathResource("fdfs_client.conf");
		ClientGlobal.init(resource.getClassLoader().getResource("fdfs_client.conf").getPath());
		//连接tracker
		TrackerClient trackerClient = new TrackerClient();
		//获得storage的地址
		TrackerServer trackerServer = trackerClient.getConnection();
		//连接storage
		StorageClient1 storageClient1 = new StorageClient1();
		//获得文件后缀
		String file_ext_name=FilenameUtils.getExtension(filename);
		//获得meta信息类似描述
		NameValuePair[] meta_list=new NameValuePair[3];
		meta_list[0]=new NameValuePair("filename", filename);
		meta_list[1]=new NameValuePair("fileext", file_ext_name);
		meta_list[2]=new NameValuePair("filesize", size+"");
		String path = storageClient1.upload_file1(pic, file_ext_name, meta_list);
//		http://192.168.200.128/
//		group1/M00/00/01/wKjIgFWOYc6APpjAAAD-qk29i78248.jpg  =path
		return path;
	}
}
