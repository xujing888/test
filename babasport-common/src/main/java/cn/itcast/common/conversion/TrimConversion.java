package cn.itcast.common.conversion;

import org.springframework.core.convert.converter.Converter;
/**
 * S:传入页面数据类型
 * T:转换后的类型
 * @author 73588
 *
 */
public class TrimConversion implements Converter<String, String>{

	@Override
	public String convert(String source) {
		try {
			if (null!=source) {
				source = source.trim();
				if (!"".equals(source)) {
					return source;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
