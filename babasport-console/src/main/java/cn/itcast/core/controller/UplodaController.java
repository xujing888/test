package cn.itcast.core.controller;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
/**
 * 上传图片
 * @author 73588
 *
 */
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;

import cn.itcast.common.web.Constants;
import cn.itcast.core.service.product.UploadService;
@Controller
public class UplodaController {

	@Autowired
	private UploadService uploadService;
	@RequestMapping(value="/upload/uploadPic.do")
	public void uploadPic(MultipartFile pic,HttpServletRequest request,HttpServletResponse reponse) throws Exception{
	/*	String originalFilename = pic.getOriginalFilename();
	//	System.out.println(originalFilename);
		//获得文件的后缀名
		String extension = FilenameUtils.getExtension(originalFilename);
		String path="/upload/"+UUID.randomUUID().toString()+"."+extension;
		//获得项目的真实路径
		String realPath = request.getSession().getServletContext().getRealPath("");
		String url=realPath+path;
		//将文件传入制定文化
		pic.transferTo(new File(url));
		//把路径转为json
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("path", path);
		reponse.setContentType("type/json;charset=utf-8");
		reponse.getWriter().write(jsonObject.toString());*/
		String filename = pic.getOriginalFilename();
		long size = pic.getSize();
		//实际应该保存在分布式文件系统
		String path = uploadService.uploadPic(pic.getBytes(), filename, size);
		String url=Constants.IMG_URL+path;
		JSONObject jsonObject = new JSONObject();
		//System.out.println(url);
		jsonObject.put("path", url);
		reponse.setContentType("type/json;charset=utf-8");
		reponse.getWriter().write(jsonObject.toString());	
	}
	//添加商品的上传商品图片多张
	@RequestMapping(value="/upload/uploadPics.do")
	@ResponseBody
	public List<String> uploadPics(@RequestParam(required=false) MultipartFile[] pics) throws  Exception{
		//创建集合存放多个图片地址
		List<String> list=new ArrayList<>();
		for (MultipartFile pic : pics) {
			String filename = pic.getOriginalFilename();
			long size = pic.getSize();
			//实际应该保存在分布式文件系统
			String path = uploadService.uploadPic(pic.getBytes(), filename, size);
			String url=Constants.IMG_URL+path;
			list.add(url);
		}
		return list;
	}
	//添加商品的富文本上传图片多张
	@RequestMapping(value="/upload/uploadFck.do")
	public void uploadFck(HttpServletRequest request ,HttpServletResponse reponse) throws  Exception{
		//万能版接受图片（消耗资源较大）
		MultipartRequest mr=(MultipartRequest)request;
		//图片支持多张上传 只有图片其他过滤
		Map<String, MultipartFile> fileMap = mr.getFileMap();
		Set<Entry<String,MultipartFile>> entrySet = fileMap.entrySet();
		for (Entry<String, MultipartFile> entry : entrySet) {
			MultipartFile pic = entry.getValue();
			String filename = pic.getOriginalFilename();
			long size = pic.getSize();
			//实际应该保存在分布式文件系统
			String path = uploadService.uploadPic(pic.getBytes(), filename, size);
			String url=Constants.IMG_URL+path;
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("url", url);
			jsonObject.put("error", 0);
			reponse.setContentType("application/json;charset=utf-8");
			reponse.getWriter().write(jsonObject.toString());	
		}
	}
		
	
}
