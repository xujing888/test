package cn.itcast.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.itcast.core.pojo.product.Sku;
import cn.itcast.core.service.product.SkuService;

/**
 * 库存
 * @author 73588
 *
 */
@Controller
public class SkuController {
	@Autowired
	private SkuService skuService;
	@RequestMapping(value="/sku/list.do")
	public String list(Long productId,Model model) {
		List<Sku> list = skuService.findSkuListById(productId);
		model.addAttribute("skus", list);
		return "sku/list";
	}
	
	@RequestMapping(value="/sku/update.do")
	@ResponseBody
	public String update(Sku sku ){
	//	System.out.println("1111111111");
		String flag="1";
		try {
			skuService.updateById(sku);
		} catch (Exception e) {
			flag="0";
			e.printStackTrace();
		}
		return flag;
	}
		

}
