package cn.itcast.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.product.Brand;
import cn.itcast.core.service.product.BrandService;

@Controller
public class BrandController {
	@Autowired
	private BrandService brandService;
	//品牌列表
	@RequestMapping(value="/brand/list.do")
	public String brandList(Integer pageNo,String name,Integer isDisplay,Model model){
	//	List<Brand> brands = brandService.findListByQuery(name, isDisplay);
		Pagination pagination = brandService.pageListByQuery(pageNo, name, isDisplay);
		model.addAttribute("pagination", pagination);
		//为了查询回显
		model.addAttribute("name", name);
		model.addAttribute("isDisplay", isDisplay);
		return "brand/list";
	}
	//修改品牌回显
	@RequestMapping(value="/brand/toEidt.do")
	public String toEidt(Integer id,Model model){
		Brand brand=brandService.findBrandById(id);
		model.addAttribute("brand", brand);
		return "brand/edit";
	}
	//修改品牌
	@RequestMapping(value="/brand/edit.do")
	public String edit(Brand brand){
	//	System.out.println(brand.getImgUrl());
		brandService.updateBrandById(brand);
		return "redirect:/brand/list.do";
	}
	//删除品牌(带条件的 分页 名称 上架)
	@RequestMapping(value="/brand/delete.do")
	public String delete(Long[] ids){
		brandService.deleteBrandById(ids);
		return "forward:/brand/list.do";
	}
	
	
}
