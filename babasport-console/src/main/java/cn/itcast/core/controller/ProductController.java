package cn.itcast.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
/**
 * 产品
 * @author 73588
 *
 */
import org.springframework.web.bind.annotation.RequestMapping;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.product.Brand;
import cn.itcast.core.pojo.product.Color;
import cn.itcast.core.pojo.product.Product;
import cn.itcast.core.service.product.BrandService;
import cn.itcast.core.service.product.ColorService;
import cn.itcast.core.service.product.ProductService;
@Controller
public class ProductController {
	@Autowired
	private BrandService brandService;
	@Autowired
	private ProductService productService;
	/**
	 * 产品条件查询
	 * @param pageNo
	 * @param isShow
	 * @param name
	 * @return
	 */
	@RequestMapping("/product/list.do")
	public String list(Integer pageNo,Boolean isShow,String name,Long brandId,Model model){
		//回显品牌表
		List<Brand> brands = brandService.findListByQuery(null, 1);
		model.addAttribute("brands", brands);
		Pagination pagination = productService.selectPageProductQuery(pageNo, isShow, name, brandId);
		model.addAttribute("pagination", pagination);
		model.addAttribute("isShow", isShow);
		model.addAttribute("name", name);
		model.addAttribute("brandId",brandId);
		return "product/list";
	}
	@Autowired
	private ColorService colorServie;
	//跳转商品添加列表
	@RequestMapping(value="/product/Toadd.do")
	public String Toadd(Model model){
		//回显商品品牌
		List<Brand> brands = brandService.findListByQuery(null, 1);
		//回显色彩
		List<Color> colors = colorServie.selectColor();
		model.addAttribute("brands",brands);
		model.addAttribute("colors",colors);
		return "product/add";
	}
	//添加商品
	@RequestMapping(value="/product/add.do")
	public String add(Product product){
		//添加商品 并且添加到库存表
		productService.addProduct(product);
		return "redirect:/product/list.do";
	}
	//商品上架
	@RequestMapping(value="/brand/isShow.do")
	public String isShow(Long[] ids,Model model){
		productService.isShowProduct( ids);
		return "forward:/product/list.do";
	}
}
