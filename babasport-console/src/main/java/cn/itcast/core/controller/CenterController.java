package cn.itcast.core.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.itcast.core.pojo.TestTb;
import cn.itcast.core.service.TestTbService;

@Controller
public class CenterController {

	@Autowired
	private TestTbService testTbService;
	//测试
	@RequestMapping(value="/test/index.do")
	public String add(){
		TestTb testTb = new TestTb();
		testTb.setName("张8");
		testTb.setBirthday(new Date());
		//testTbService.insertTestTb(testTb);
		return "index";
	}
	//首页显示index
	@RequestMapping(value="/console/index.do")
	public String index(){
		return "index";
	}
	//首页显示 main
	@RequestMapping(value="/console/main.do")
	public String main(){
		return "main";
	}
	//首页显示 top
	@RequestMapping(value="/console/top.do")
	public String top(){
		return "top";
	}
	//首页显示 mian  left
	@RequestMapping(value="/console/left.do")
	public String left(){
		return "left";
	}
	//首页显示 main right
	@RequestMapping(value="/console/right.do")
	public String right(){
		return "right";
	}
	//商品显示 product_main
	@RequestMapping(value="/console/frame/product_main.do")
	public String product_main(){
		return "frame/product_main";
	}
		
	//商品显示 product_left
	@RequestMapping(value="/console/frame/product_left.do")
	public String product_left(){
		return "frame/product_left";
	}
	
	//广告显示 frame/ad_main.do
	@RequestMapping(value="/console/frame/ad_main.do")
	public String ad_main(){
		return "frame/ad_main";
	}
	//商品显示 ad_left
	@RequestMapping(value="/console/frame/ad_left.do")
	public String ad_left(){
		return "frame/ad_left";
	}
}
