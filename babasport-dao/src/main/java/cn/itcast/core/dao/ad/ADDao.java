package cn.itcast.core.dao.ad;

import cn.itcast.core.pojo.ad.AD;
import cn.itcast.core.pojo.ad.ADQuery;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ADDao {
    int countByExample(ADQuery example);

    int deleteByExample(ADQuery example);

    int deleteByPrimaryKey(Long id);

    int insert(AD record);

    int insertSelective(AD record);

    List<AD> selectByExample(ADQuery example);

    AD selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") AD record, @Param("example") ADQuery example);

    int updateByExample(@Param("record") AD record, @Param("example") ADQuery example);

    int updateByPrimaryKeySelective(AD record);

    int updateByPrimaryKey(AD record);
}