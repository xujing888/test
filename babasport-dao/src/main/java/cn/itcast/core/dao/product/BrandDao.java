package cn.itcast.core.dao.product;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.itcast.core.pojo.product.Brand;
import cn.itcast.core.pojo.product.BrandQuery;

public interface BrandDao {

	//条件查询品牌列表
	public List<Brand> findListByQuery(BrandQuery brandQuery);
	//查询brand总条数
	public Integer countBrandByQuery(BrandQuery brandQuery);
	//根据id查询
	public Brand findBrandById(Integer id);
	//根据id修改
	public void updateBrandById(Brand brand);
	//根据id删除
	public void deleteBrandById(Long[] ids);
	
	
	 	int countByExample(BrandQuery example);

	    int deleteByExample(BrandQuery example);

	    int deleteByPrimaryKey(Long id);

	    int insert(Brand record);

	    int insertSelective(Brand record);

	    List<Brand> selectByExample(BrandQuery example);

	    Brand selectByPrimaryKey(Long id);

	    int updateByExampleSelective(@Param("record") Brand record, @Param("example") BrandQuery example);

	    int updateByExample(@Param("record") Brand record, @Param("example") BrandQuery example);

	    int updateByPrimaryKeySelective(Brand record);

	    int updateByPrimaryKey(Brand record);
}
