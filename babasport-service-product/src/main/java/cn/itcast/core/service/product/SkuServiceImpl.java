package cn.itcast.core.service.product;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.core.dao.product.ColorDao;
import cn.itcast.core.dao.product.SkuDao;
import cn.itcast.core.pojo.product.Color;
import cn.itcast.core.pojo.product.Sku;
import cn.itcast.core.pojo.product.SkuQuery;

/**
 * 库存
 * @author 73588
 *
 */
@Service("skuService")
@Transactional
public class SkuServiceImpl implements SkuService {

	@Autowired
	private SkuDao skuDao;
	@Autowired
	private ColorDao colorDao;
	public List<Sku> findSkuListById(Long productId){
		SkuQuery query = new SkuQuery();
		query.createCriteria().andProductIdEqualTo(productId);
		List<Sku> list = skuDao.selectByExample(query);
		for (Sku sku : list) {
			//查询出颜色id
			Long colorId = sku.getColorId();
			//获取颜色对象
			Color color = colorDao.selectByPrimaryKey(colorId);
			//给颜色
			sku.setColor(color);
		}
		return list;
	}
	@Override
	public void updateById(Sku sku) {
		skuDao.updateByPrimaryKeySelective(sku);
	}
	
}
