package cn.itcast.core.service.product;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.dao.product.ProductDao;
import cn.itcast.core.dao.product.SkuDao;
import cn.itcast.core.pojo.product.Product;
import cn.itcast.core.pojo.product.ProductQuery;
import cn.itcast.core.pojo.product.ProductQuery.Criteria;
import cn.itcast.core.pojo.product.Sku;
import cn.itcast.core.pojo.product.SkuQuery;
import redis.clients.jedis.Jedis;

@Service("productService")
@Transactional
public class ProductServiceImpl implements ProductService{

	@Autowired
	private ProductDao productDao;
	
	//分页 条件显示产品列表
	public Pagination selectPageProductQuery(Integer pageNo,Boolean isShow,String name ,Long brandId){
		ProductQuery productQuery = new ProductQuery();
		Criteria createCriteria = productQuery.createCriteria();
		StringBuilder param= new StringBuilder();
		if (null!=name) {
			createCriteria.andNameLike("%"+name+"%");
			param.append("name=").append(name);
		}
		if (null !=brandId) {
			createCriteria.andBrandIdEqualTo(brandId);
			param.append("&brandId=").append(brandId);
		}
		if (null !=isShow) {
			createCriteria.andIsShowEqualTo(isShow);
			param.append("&isShow=").append(isShow);
		}else {
			createCriteria.andIsShowEqualTo(false);
			param.append("&isShow=").append(false);
		}
		//设置首页
		productQuery.setPageNo(Pagination.cpn(pageNo));
		//设置每页显示数
		productQuery.setPageSize(5);
		//排序  倒序查询
		productQuery.setOrderByClause("id desc");
		
		Pagination pagination = new Pagination();
		pagination.setPageNo(productQuery.getPageNo());
		pagination.setPageSize(productQuery.getPageSize());
		pagination.setTotalCount(productDao.countByExample(productQuery));
		pagination.setList(productDao.selectByExample(productQuery));
		
		String url="/product/list.do";
		pagination.pageView(url, param.toString());
		return pagination;
	}

	@Autowired
	private SkuDao skuDao;
	@Autowired
	private Jedis jedis;
	//添加商品
	public void addProduct(Product product) {
		//产品id自增长
		Long incr = jedis.incr("pno");
		product.setId(incr);
		//没填写的字段手动设置
		//不删除
		product.setIsDel(false);
		//不上架
		product.setIsShow(false);
		//录入时间
		product.setCreateTime(new Date());
		productDao.insertSelective(product);
		
		//设置productDao.xml 插入后获得自增主键
		String colors = product.getColors();
		String sizes = product.getSizes();
		//切割颜色集和尺码集
		String[] col = colors.split(",");
		String[] siz = sizes.split(",");
		for (String color : col) {
			for (String size : siz) {
				Sku sku = new Sku();
				//设置产品id
				sku.setProductId(product.getId());
				//颜色id
				sku.setColorId(Long.parseLong(color));
				//尺寸
				sku.setSize(size);
				//价格
				sku.setPrice(0f);
				//运费
				sku.setDeliveFee(10f);
				//库存
				sku.setStock(0);
				//限购
				sku.setUpperLimit(200);
				//创建时间
				sku.setCreateTime(new Date());
				//添加到库存表
				skuDao.insertSelective(sku);
			}
		}
		
	}
	@Autowired
	private SolrServer  solrServer ;
	@Override
	public void isShowProduct(Long[] ids) {
		Product product= new Product();
		for (Long id : ids) {
			product.setId(id);
			//设置状态
			product.setIsShow(true);
			//更新
			productDao.updateByPrimaryKeySelective(product);
			//将上架的货物导入索引库
			SolrInputDocument doc = new SolrInputDocument();
			//添加商品id到索引库
			doc.setField("id", id);
			Product p = productDao.selectByPrimaryKey(id);
			//商品名称
			doc.setField("name_ik", p.getName());
			//品牌id
			doc.setField("brandId", p.getBrandId());
			//图片 url
			doc.setField("url", p.getImgUrl());
			//价格最低的那个 select price from table where pid= a order by asc limit 0,1
			SkuQuery skuQuery= new SkuQuery();
			skuQuery.createCriteria().andIdEqualTo(id);
			skuQuery.setOrderByClause("price asc");
			skuQuery.setPageNo(1);
			skuQuery.setPageSize(1);
			//设置只查询price
			skuQuery.setFields("price");
			List<Sku> list = skuDao.selectByExample(skuQuery);
			doc.setField("price", list.get(0).getPrice());
			//设置时间
			doc.setField("last_modified", new Date());
			try {
				solrServer.add(doc, 1000);
			} catch (SolrServerException | IOException e) {
				e.printStackTrace();
			}
		}
	}
}
