package cn.itcast.core.service.product;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.dao.product.BrandDao;
import cn.itcast.core.pojo.product.Brand;
import cn.itcast.core.pojo.product.BrandQuery;

@Service("brandService")
@Transactional
public class BrandServiceImpl implements BrandService {

	@Autowired
	private BrandDao brandDao;

	@Override
	public List<Brand> findListByQuery(String name, Integer isDisplay) {
		//创建对象
		BrandQuery brandQuery = new BrandQuery();
		//判断name
		if (null!=name) {
			brandQuery.setName(name);
		}
		//判断isDisplay
		if (null!=isDisplay) {
			brandQuery.setIsDisplay(isDisplay);
		}else {
			brandQuery.setIsDisplay(1);
		}
		List<Brand> ListBrand = brandDao.findListByQuery(brandQuery);
		return ListBrand;
	}
	@Override
	public Pagination pageListByQuery(Integer pageNo,String name, Integer isDisplay) {
		//带条件的分页
		//创建对象
		BrandQuery brandQuery = new BrandQuery();
		StringBuilder params = new StringBuilder();
		//判断name
		if (null!=name) {
			brandQuery.setName(name);
			params.append("name=").append(name);
		}
		//判断isDisplay
		if (null!=isDisplay) {
			brandQuery.setIsDisplay(isDisplay);
			params.append("&isDisplay=").append(isDisplay);
		}else {
			brandQuery.setIsDisplay(1);
			params.append("&isDisplay=").append(1);
		}
		//设置每页显示数
		brandQuery.setPageSize(5);
		//设置当前页Pagination.cpn(pageNo)这是分页工具里的
		brandQuery.setPageNo(Pagination.cpn(pageNo));
		//获得分页对象（含有集合）
		Pagination pagination = new Pagination();
		pagination.setPageNo(brandQuery.getPageNo());
		pagination.setPageSize(brandQuery.getPageSize());
		int totalCount=brandDao.countBrandByQuery(brandQuery);
		pagination.setTotalCount(totalCount);
		List<Brand> list = brandDao.findListByQuery(brandQuery);
		pagination.setList(list);
		///上面分页数据完整     /brand/list.do?name=依&isDisplay=1
		//分页显示在页面 吧数据封装给 pagination
		String url="/brand/list.do";
		pagination.pageView(url, params.toString());
		return pagination;
	}
	@Override
	public Brand findBrandById(Integer id) {
		return brandDao.findBrandById( id);
	}
	@Override
	public void updateBrandById(Brand brand) {
		brandDao.updateBrandById( brand);
		
	}
	@Override
	public void deleteBrandById(Long[] ids) {
		brandDao.deleteBrandById( ids);
	}
	
	
}
