package cn.itcast.core.service.product;

import org.springframework.stereotype.Service;

import cn.itcast.common.fdfs.FdfsUtils;

/**
 * 图片上传
 * @author 73588
 *
 */
@Service("uploadService")
public class UploadServiceImpl implements UploadService {

	public String uploadPic(byte[] pic,String filename ,Long size) throws Exception{
		String path = FdfsUtils.uploadPic(pic, filename, size);
		return path;
		
	}
}
