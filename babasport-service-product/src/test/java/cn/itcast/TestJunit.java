package cn.itcast;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.itcast.core.dao.TestTbDao;
import cn.itcast.core.dao.product.ProductDao;
import cn.itcast.core.pojo.TestTb;
import cn.itcast.core.pojo.product.Product;
import cn.itcast.core.pojo.product.ProductQuery;
import cn.itcast.core.service.TestTbService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:application-context.xml"})
public class TestJunit {

	@Autowired
	private TestTbDao testTbDao;
	/*@Autowired
	private TestTbService testTbService;*/
	@Test
	public void test1() throws Exception{
		TestTb testTb = new TestTb();
		testTb.setName("张71");
		testTb.setBirthday(new Date());
		testTbDao.insertTestTb(testTb);
	}
	@Autowired
	private ProductDao productDao;
	
	@Test
	public void test2() throws Exception{
		Product product = productDao.selectByPrimaryKey(1L);
		System.out.println(product);
	}
	@Test
	public void test3() throws Exception{
		//条件查询 排序 分页 指定字段
		ProductQuery productQuery = new ProductQuery();
		//条件
		productQuery.createCriteria().andBrandIdEqualTo(7L).andNameLike("%包%");
		List<Product> list = productDao.selectByExample(productQuery);
		for (Product product : list) {
			System.out.println(product);
		}
	}
}
