package cn.itcast.core.service.product;

import java.util.List;

import cn.itcast.core.pojo.product.Color;

public interface ColorService {
	//查询色彩集
	public List<Color> selectColor();

}
