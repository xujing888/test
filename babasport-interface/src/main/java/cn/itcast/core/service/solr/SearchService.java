package cn.itcast.core.service.solr;

import org.apache.solr.client.solrj.SolrServerException;

import cn.itcast.common.page.Pagination;

public interface SearchService {

	public Pagination QuerList(Integer pageNo,String keyword) throws SolrServerException;
}
