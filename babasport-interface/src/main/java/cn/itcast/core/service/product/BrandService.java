package cn.itcast.core.service.product;

import java.util.List;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.product.Brand;

public interface BrandService {

	//条件查询品牌列表
	public List<Brand> findListByQuery(String name,Integer isDisplay);
	//分页查询品牌列表
	public Pagination pageListByQuery(Integer pageNo,String name, Integer isDisplay);
	//根据id查询
	public Brand findBrandById(Integer id);
	//根据id修改
	public void updateBrandById(Brand brand);
	//根据id删除
	public void deleteBrandById(Long[] ids);	
}
