package cn.itcast.core.service.product;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.pojo.product.Product;

public interface ProductService {
	//分页 显示产品
	public Pagination selectPageProductQuery(Integer pageNo,Boolean isShow,String name,Long brandId);
	//添加商品
	public void addProduct(Product product);
	//上架商品
	public void isShowProduct(Long[] ids);

}
