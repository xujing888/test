package cn.itcast.core.service.product;

import java.util.List;

import cn.itcast.core.pojo.product.Sku;

public interface SkuService {
	//根据brandId查询库存表
	public List<Sku> findSkuListById(Long productId);

	public void updateById(Sku sku);
}
