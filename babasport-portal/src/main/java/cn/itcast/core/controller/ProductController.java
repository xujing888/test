package cn.itcast.core.controller;

import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.itcast.common.page.Pagination;
import cn.itcast.core.service.solr.SearchService;

/**
 * 前台商品管理
 * @author 73588
 *
 */
@Controller
public class ProductController {

	
	//首页展示 直接登陆 localhost:8082  value的/可以省区
	@RequestMapping(value="")
	public String toList(Model model){
		return "index";
	}
	@Autowired
	private SearchService searchService;
	//关键字查询索引库 (下面的分页查询上面的条件不动而家的pageNo)
	@RequestMapping(value="/Search")
	public String  searchSolr(String keyword,Integer pageNO,Model model) throws SolrServerException{
		Pagination pagination = searchService.QuerList(pageNO, keyword);
		model.addAttribute("pagination", pagination);
		model.addAttribute("keyword", keyword);
		return "search";
	}
}
